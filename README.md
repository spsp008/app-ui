
# Shopping Cart-UI


## Environment
- Angular 9

## Cloud service used
- Amazon EC2 for API
- Amazon RDS for Postgres DB
- Amazon S3 for UI files static hosting

## Installation

Use the package manager [npm](https://www.npmjs.com/) to install dependancies. In the root folder run below command.

```bash
npm install
```

## Usage
To start app

```bash
npm start
```

