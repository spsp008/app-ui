import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ItemsComponent } from './modules/items/components/items.component';
import { AuthGuardService } from './modules/login/auth.guard';
import { LoginComponent } from './modules/login/login.component';


const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'items', component: ItemsComponent, canActivate: [AuthGuardService]},
  {path: '**', redirectTo: '/items'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
