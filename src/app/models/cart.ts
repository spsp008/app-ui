export interface Cart {
  id: number;
  user_id: number;
  created_at: Date;
}
