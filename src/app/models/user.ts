export interface User {
  id: number;
  name: string;
  user_name: string;
  password: string;
  token: string;
  cart_id: number;
  createdAt: Date;
}
