import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Cart } from 'src/app/models/cart';
import { Item } from 'src/app/models/item';
import { User } from 'src/app/models/user';
import { CartService } from 'src/app/services/cart.service';
import { ItemsService } from 'src/app/services/items.service';
import { LoginService } from 'src/app/services/login.service';
import { OrderService } from 'src/app/services/order.service';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.scss']
})
export class ItemsComponent implements OnInit {
  public items$: Observable<Array<Item>>;
  public cartItems$: Observable<any>;
  public cartItems: any;
  public items: Array<Item>;
  public itemsMap;
  public user: User;
  public cart: Cart;
  public addedItems: Array<Item> = [];
  constructor(
    private _itemsService: ItemsService,
    private _loginService: LoginService,
    private _cartService: CartService,
    private _ordersService: OrderService
  ) {
    this.items$ = _itemsService.getItems();
    this.cartItems$ = _cartService.getCartItems();
    this._loginService.currentUser.subscribe(cu => this.user = cu);
  }

  ngOnInit() {
    this.items$.subscribe(items => {
      this.items = items;
      this.itemsMap = this.items.reduce((acc, curr) => {
        acc[curr.id] = curr;
        return acc;
      }, {});
    });
    this.cartItems$.subscribe(cItems => {
      this.addedItems = [];
      cItems.forEach(ci => {
        this.addedItems.push(this.itemsMap[ci.item_id]);
      });
    })
  }

  addToCart(item: Item) {
    this._cartService.addItem({cart_id: this.user.cart_id, item_id: item.id}).subscribe(res => {
      if (res.status) {
        this.addedItems.push(item);
      }
    });
  }

  checkout() {
    this._cartService.completeOrder(this.user.cart_id).subscribe(res => {
      if (res.status) {
        alert('Order placed successfully');
        this.addedItems = [];
      }
    });
  }

  viewCart() {
    if (!this.addedItems.length) return;
    const items = this.addedItems.map(item => item.name).join('\n');
    alert(items);
  }

  showHistory() {
    this._ordersService.getOrders().subscribe(res => {
      if (res.status) {
        const {orders} = res;
        const stringArray = [];
        if (orders.length) {
          orders.forEach((order, index) => {
            stringArray.push(`Order #${index + 1}`);
            order.CartItems.forEach(ci => {
              const item = this.itemsMap[ci.item_id];
              stringArray.push(`${item.name}: ${ci.quantity} unit(s)`);
            });
            stringArray.push('\n');
          })
        }
        if (!stringArray.length) {
          alert('No orders!');
        } else {
          alert(stringArray.join('\n'));
        }
      }
    })
  }

  logout() {
    this._loginService.logout().subscribe(res => {
      if (res.status) {
        alert('You are logged out successfully!')
      }
    });
  }

}
