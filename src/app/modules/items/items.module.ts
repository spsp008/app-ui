import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ItemsComponent } from './components/items.component';

@NgModule({
  declarations: [
    ItemsComponent
  ],
  imports: [CommonModule],
  providers: []
})
export class ItemsModule { }
