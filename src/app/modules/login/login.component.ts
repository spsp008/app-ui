import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(public router: Router, private _loginService: LoginService) {
    if (this._loginService.currentUserValue) {
      this.router.navigate(['/items']);
    }
  }
  public isLogin = true;
  public user_name: string;
  public password: string;
  public name: string;

  ngOnInit() {

  }

  login() {
    const {user_name, password} =  this;
    this._loginService.login({user_name, password}).subscribe(res => {
      if (res.status) {
        this.router.navigate(['/items']);
      } else {
        alert(res.message);
      }
    });
  }

  signUp() {
    const {name, user_name, password} =  this;
    if (!password || password.length < 8) {
      alert('Password length must be greater than equal to 8');
      return;
    }
    this._loginService.signup({name, user_name, password}).subscribe(res => {
      if (res.status)  {
        this.router.navigate(['/items']);
        this.isLogin = true;
      } else {
        alert(res.message);
      }
    });
  }
}
