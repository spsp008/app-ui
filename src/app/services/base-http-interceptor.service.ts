import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpErrorResponse,
  HTTP_INTERCEPTORS,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { LoginService } from './login.service';

@Injectable({
  providedIn: 'root',
})
export class BaseHttpInterceptorService implements HttpInterceptor {
  constructor(private router: Router, private loginService: LoginService) {}
  private authToken = localStorage.getItem('accessToken');
  private handleError = (error: HttpErrorResponse) => {
    if (error.status === 401) {
      this.loginService.logout();
    }
    return throwError(error.error);
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const authToken = localStorage.getItem('accessToken');
    this.authToken = authToken;
    if (this.authToken) {
      const modifiedRequest = req.clone({
        headers: req.headers.set('x-access-token', this.authToken),
      });
      return next.handle(modifiedRequest).pipe(catchError(this.handleError));
    } else {
      return next.handle(req).pipe(catchError(this.handleError));
    }
  }
}

export const API_INTERCEPTOR_PROVIDER = {
  provide: HTTP_INTERCEPTORS,
  useClass: BaseHttpInterceptorService,
  multi: true
};
