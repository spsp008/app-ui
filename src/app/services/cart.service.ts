import { Injectable } from '@angular/core';
import { BaseHttpService } from './base-http.service';
@Injectable({
  providedIn: 'root',
})
export class CartService {
  constructor(private baseApi: BaseHttpService) {}

  addItem(data: {item_id: number, cart_id: number}): any {
    return this.baseApi.post(`/cart/add`, data);
  }

  getCartItems() {
    return this.baseApi.get(`/cart/items/list`);
  }

  completeOrder(cart_id: number): any {
    return this.baseApi.post(`/cart/${cart_id}/complete`, {});
  }

}
