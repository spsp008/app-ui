import { Injectable } from '@angular/core';
import { BaseHttpService } from './base-http.service';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../models/user';
import { Item } from '../models/item';

@Injectable({
  providedIn: 'root',
})
export class ItemsService {
  constructor(private baseApi: BaseHttpService) {}

  getItems(): any {
    return this.baseApi.get(`/item/list`);
  }

  signup(data: Partial<User>): any {
    return this.baseApi.post(`/user/create`, data);
  }

}
