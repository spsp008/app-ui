import { Injectable } from '@angular/core';
import { BaseHttpService } from './base-http.service';
@Injectable({
  providedIn: 'root',
})
export class OrderService {
  constructor(private baseApi: BaseHttpService) {}

  getOrders(): any {
    return this.baseApi.get(`/order/list`);
  }

}
